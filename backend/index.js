const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());



const db = mysql.createConnection({
  user: "root",
  host: "localhost",
  password: "",
  database: "test",
});



app.get("/person", (req, res) => {
    db.query("SELECT * FROM tbl_user LEFT JOIN tbl_section on tbl_user.section_id = tbl_section.section_id ", (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
});

app.get("/newuser", (req, res) => {
  db.query("SELECT * FROM tam ", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.get("/agency", (req, res) => {
  db.query("SELECT * FROM tbl_section", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});



app.post("/createagency", (req, res) => {
  const section_name = req.body.section_name;
  db.query(
    "INSERT INTO tbl_section (section_name,section_flag) VALUES (?,1)",
    [section_name],
    (err, result) => {
      if (err) {
        console.log('5',err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
  console.log('name',section_name)
});

app.put("/update", (req, res) => {
  const id = req.body.id;
  const wage = req.body.wage;
  db.query(
    "UPDATE employees SET wage = ? WHERE id = ?",
    [wage, id],
    (err, result) => {
      if (err) {
        console.log('4',err);
      } else {
        res.send(result);
      }
    }
  );
});



app.put("/updateagency/:section_id", (req, res) => {
  const section_id = req.params.section_id;
  const section_name = req.body.section_name;
  db.query(
    "UPDATE tbl_section SET section_name = ?  WHERE section_id = ?",
    [section_name,section_id], 
    (err, result) => {
      if (err) {
        console.log('3',err)
      } else {
        res.send(result)
      }
    }
  )
  console.log('ghhjj',section_id)
  console.log('ghjj',section_name)
})

app.post("/create", (req, res) => {
  const secid = req.body.section_id
  const user = req.body.username
  const pas = req.body.password
  const fname = req.body.fname
  const lname = req.body.lname
  const telno = req.body.telno
  const email = req.body.email
  const director = req.body.director
  const manager = req.body.manager
  const supervisor = req.body.supervisor
  const supplies = req.body.supplies
  const responsible = req.body.responsible
  const admin = req.body.admin
  const flag = req.body.flag
  console.log(req.body)
  db.query(
    "INSERT INTO tbl_user (section_id,username,password,fname,lname,telno,email,director,manager,supervisor,supplies,responsible,admin,flag) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
    [secid,user,pas,fname,lname,telno,email,director,manager,supervisor,supplies,responsible,admin,flag],
    (err, result) => {
      if (err) {
        console.log('5',err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
  // console.log('name',section_name)
});

app.put("/updateuser/:ID", (req, res) => {
  const id = req.params.ID;
  const secid = req.body.section_id
  const director = req.body.director
  const manager = req.body.manager
  const supervisor = req.body.supervisor
  const supplies = req.body.supplies
  const responsible = req.body.responsible
  const admin = req.body.admin
  db.query(
    "UPDATE tbl_user SET section_id = ? ,director = ? ,manager = ? ,supervisor = ? ,supplies = ? ,responsible = ? ,admin = ?   WHERE user_id = ?",
    [secid,director,manager,supervisor,supplies,responsible,admin,id], 
    (err, result) => {
      if (err) {
        console.log('3',err)
      } else {
        res.send(result)
      }
    }
  )
  console.log('ghhjj',secid)
  console.log('ghjj',director)
})

app.delete("/delete/:p_id", (req, res) => {
  const p_id = req.params.p_id;
  db.query("DELETE FROM tbl_user WHERE user_id = ?", p_id, (err, result) => {
    if (err) {
      console.log('2',err);
    } else {
      res.send(result);
    }
  });
});

app.delete("/delete2/:a_id", (req, res) => {
  const a_id = req.params.a_id;
  db.query("DELETE FROM tbl_section WHERE section_id = ?", a_id, (err, result) => {
    if (err) {
      console.log('1',err);
    } else {
      res.send(result);
    }
  });
});
/////ยุท ปีงบ
app.get("/strategic", (req, res) => {
  db.query("SELECT * FROM tbl_fiscalyear ", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.post("/createstrategic", (req, res) => {
  const year = req.body.fiscalyear
  const plan = req.body.plan_name
  db.query(
    "INSERT INTO tbl_fiscalyear (fiscalyear,plan_name,flag) VALUES (?,?,1)",
    [year,plan],
    (err, result) => {
      if (err) {
        console.log('5',err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
  console.log('ghhjj',year)
  console.log('ghjj',plan)
  
});

app.delete("/deletestrategic/:f_id", (req, res) => {
  const delete_id = req.params.f_id;
  db.query("DELETE FROM tbl_fiscalyear WHERE fiscalyear_id = ?", delete_id, (err, result) => {
    if (err) {
      console.log('1',err);
    } else {
      res.send(result);
    }
  });
  console.log('ghjj',delete_id)
});

app.put("/updatestrategic/:f_id", (req, res) => {
  const ID = req.params.f_id
  const name = req.body.plan_name
  const  time1 = req.body.director_of_time
  const date1 = req.body.director_of_date
  const time2 = req.body.ref_of_time
  const date2 = req.body.ref_of_date
  db.query(
    "UPDATE tbl_fiscalyear SET plan_name = ? , director_of_time= ? , director_of_date= ? , ref_of_time= ? , ref_of_date= ?  WHERE fiscalyear_id = ?",
    [name,time1,date1,time2,date2,ID], 
    (err, result) => {
      if (err) {
        console.log('3',err)
      } else {
        res.send(result)
      }
    }
  )
  
})

app.put("/updatesstatus/:ID", (req, res) => {
  const ID = req.params.ID;
  const flag = req.body.flag;
  db.query(
    "UPDATE tbl_fiscalyear SET flag = ?  WHERE fiscalyear_id = ?",
    [flag,ID], 
    (err, result) => {
      if (err) {
        console.log('3',err)
      } else {
        res.send(result)
      }
    }
  )
  console.log('ghhjj',ID)
  console.log('ghjj',flag)
})

////ประเด็นยุท
app.get("/strategicid", (req, res) => {
  db.query("SELECT * FROM  tbl_strategic LEFT JOIN tbl_fiscalyear on tbl_strategic.fiscalyear_id = tbl_fiscalyear.fiscalyear_id ", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});
app.delete("/deletestid/:ID", (req, res) => {
  const delete_id = req.params.ID;
  db.query("DELETE FROM tbl_strategic WHERE strategic_id = ?", delete_id, (err, result) => {
    if (err) {
      console.log('1',err);
    } else {
      res.send(result);
    }
  });
  console.log('ghjj',delete_id)
});

app.post("/createstid", (req, res) => {
  const year = req.body.fiscalyear_id
  const order = req.body.order_strategic
  const name = req.body.strategic_name
  db.query(
    "INSERT INTO tbl_strategic (fiscalyear_id,order_strategic,strategic_name) VALUES (?,?,?)",
    [year,order,name],
    (err, result) => {
      if (err) {
        console.log('5',err);
      } else {
        res.send("Values Inserted");
      }
    }
  );
  console.log('ghhjj',year)
  console.log('ghjj',order)
  console.log('ghjj',name)
  
});
app.put("/updatest/:ID", (req, res) => {
  const ID = req.params.ID;
  const name = req.body.strategic_name;
  db.query(
    "UPDATE tbl_strategic SET strategic_name = ?  WHERE strategic_id = ?",
    [name,ID], 
    (err, result) => {
      if (err) {
        console.log('3',err)
      } else {
        res.send(result)
      }
    }
  )
  console.log('2',ID)
  console.log('2',name)
})





app.listen(3001, () => {
    console.log("Yey, your server is running on port 3001");
});



// const express = require('express');
// const path = require('path');
// const cookieSession = require('cookie-session');
// const bcrypt = require('bcrypt');
// const dbConnection = require('./config/Database');
// const { body, validationResult } = require('express-validator');

// const app = express();
// app.use(express.urlencoded({extended:false}));

// // SET OUR VIEWS AND VIEW ENGINE
// app.set('views', path.join(__dirname,'views'));
// app.set('view engine','ejs');

// // APPLY COOKIE SESSION MIDDLEWARE
// app.use(cookieSession({
//     name: 'session',
//     keys: ['key1', 'key2'],
//     maxAge:  3600 * 1000 // 1hr
// }));

// // DECLARING CUSTOM MIDDLEWARE
// const ifNotLoggedin = (req, res, next) => {
//     if(!req.session.isLoggedIn){
//         return res.render('login-register');
//     }
//     next();
// }
// const ifLoggedin = (req,res,next) => {
//     if(req.session.isLoggedIn){
//         return res.redirect('/home');
//     }
//     next();
// }
// // END OF CUSTOM MIDDLEWARE
// // ROOT PAGE
// app.get('/', ifNotLoggedin, (req,res,next) => {
//     dbConnection.execute("SELECT `name` FROM `users` WHERE `id`=?",[req.session.userID])
//     .then(([rows]) => {
//         res.render('home',{
//             name:rows[0].name
//         });
//     });
    
// });// END OF ROOT PAGE


// // REGISTER PAGE
// app.post('/register', ifLoggedin, 
// // post data validation(using express-validator)
// [
//     body('user_email','Invalid email address!').isEmail().custom((value) => {
//         return dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [value])
//         .then(([rows]) => {
//             if(rows.length > 0){
//                 return Promise.reject('This E-mail already in use!');
//             }
//             return true;
//         });
//     }),
//     body('user_name','Username is Empty!').trim().not().isEmpty(),
//     body('user_pass','The password must be of minimum length 6 characters').trim().isLength({ min: 6 }),
// ],// end of post data validation
// (req,res,next) => {

//     const validation_result = validationResult(req);
//     const {user_name, user_pass, user_email} = req.body;
//     // IF validation_result HAS NO ERROR
//     if(validation_result.isEmpty()){
//         // password encryption (using bcryptjs)
//         bcrypt.hash(user_pass, 12).then((hash_pass) => {
//             // INSERTING USER INTO DATABASE
//             dbConnection.execute("INSERT INTO `users`(`name`,`email`,`password`) VALUES(?,?,?)",[user_name,user_email, hash_pass])
//             .then(result => {
//                 res.send(`your account has been created successfully, Now you can <a href="/">Login</a>`);
//             }).catch(err => {
//                 // THROW INSERTING USER ERROR'S
//                 if (err) return  err;
//             });
//         })
//         .catch(err => {
//             // THROW HASING ERROR'S
//             if (err) return err;
//         })
//     }
//     else{
//         // COLLECT ALL THE VALIDATION ERRORS
//         let allErrors = validation_result.errors.map((error) => {
//             return error.msg;
//         });
//         // REDERING login-register PAGE WITH VALIDATION ERRORS
//         res.render('login-register',{
//             register_error:allErrors,
//             old_data:req.body
//         });
//     }
// });// END OF REGISTER PAGE


// // LOGIN PAGE
// app.post('/', ifLoggedin, [
//     body('user_email').custom((value) => {
//         return dbConnection.execute('SELECT email FROM tbl_user WHERE email=?', [value])
//         .then(([rows]) => {
//             if(rows.length == 1){
//                 return true;
                
//             }
//             return Promise.reject('Invalid Email Address!');
            
//         });
//     }),
//     body('user_pass','Password is empty!').trim().not().isEmpty(),
// ], (req, res) => {
//     const validation_result = validationResult(req);
//     const {user_pass, user_email} = req.body;
//     if(validation_result.isEmpty()){
        
//         dbConnection.execute("SELECT * FROM `tbl_user` WHERE `email`=?",[user_email])
//         .then(([rows]) => {
//             bcrypt.compare(user_pass, rows[0].password).then(compare_result => {
//                 if(compare_result === true){
//                     req.session.isLoggedIn = true;
//                     req.session.userID = rows[0].id;

//                     res.redirect('/');
//                 }
//                 else{
//                     res.render('login-register',{
//                         login_errors:['Invalid Password!']
//                     });
//                 }
//             })
//             .catch(err => {
//                 if (err) return err;
//             });


//         }).catch(err => {
//             if (err) return err;
//         });
//     }
//     else{
//         let allErrors = validation_result.errors.map((error) => {
//             return error.msg;
//         });
//         // REDERING login-register PAGE WITH LOGIN VALIDATION ERRORS
//         res.render('login-register',{
//             login_errors:allErrors
//         });
//     }
// });
// // END OF LOGIN PAGE

// // LOGOUT
// app.get('/logout',(req,res)=>{
//     //session destroy
//     req.session = null;
//     res.redirect('/');
// });
// // END OF LOGOUT

// app.use('/', (req,res) => {
//     res.status(404).send('<h1>404 Page Not Found!</h1>');
// });

// app.listen(3001, () => console.log("Server is Running..."));