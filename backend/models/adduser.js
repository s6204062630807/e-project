import db from '../db/dbConfig'

import {DataTypes} from 'sequelize'

const person = db.define({
    p_ID: {
        type: DataTypes.INTEGER
    },
    p_firstname: {
        type: DataTypes.STRING
    },
    p_lastname : {
        type: DataTypes.STRING
    },
    p_email :{
        type: DataTypes.STRING
    }

})
export default person

// module.exports = (sequelize, DataTypes) => {
    
//     const adduser = sequelize.define("adduser", {
//         p_ID: {
//             type: DataTypes.INTEGER
//         },
//         p_firstname: {
//             type: DataTypes.STRING
//         },
//         p_lastname : {
//             type: DataTypes.STRING
//         },
//         p_email :{
//             type: DataTypes.STRING
//         }
    
//     })

//     return adduser

// }