module.exports = (sequelize, DataTypes) => {

    const agency = sequelize.define("agency", {
        a_id: {
            type: DataTypes.INTEGER
        },
        a_name: {
            type: DataTypes.STRING,
            allowNull: false
        }
    
    })

    return agency

}