// const Sequelize = require("sequelize")
// const db = require("../config/Database.js")
 
// const { DataTypes } = Sequelize;
 

// module.exports = (sequelize, Sequelize) => {
//     const Tutorial = sequelize.define('tbl_user',{
//     username: {
//         type: DataTypes.STRING,
//         allowNull: false
//     },
//     fname: {
//         type: DataTypes.STRING
//     },
//     lname: {
//         type: DataTypes.STRING
//     },
//     email: {
//         type: DataTypes.STRING
//     },
//     refresh_token:{
//         type: DataTypes.TEXT
//     }
// },{
//     freezeTableName:true
// });
module.exports = (sequelize, Sequelize) => {
    const Tutorial = sequelize.define("tbl_user", {
      username: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      refresh_token: {
        type: Sequelize.TEXT
      }
    });
  
    return Tutorial;
  };


