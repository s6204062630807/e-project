import "primeicons/primeicons.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import { Button } from "primereact/button";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Calendar } from "primereact/calendar";
import { InputText } from "primereact/inputtext"
import { RadioButton } from 'primereact/radiobutton';
import { Card } from "primereact/card";
import { InputTextarea } from 'primereact/inputtextarea'
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import TableRows from "./TableRows"
import Tabindic from "./Tabindic"
import Tabbenefit from "./Tabbenefit";
import Tabobject from "./Tabobject"
import moment from "moment";

const Newproject = () => {
  const [value1,setValue1] = useState()
  const [year, setYear] = useState()
  const [typpj,setTyppj] = useState()
  const [updatepj,setUpdatepj] = useState()
  const [have,setHave] = useState()
 

  const yearformat = moment(year).format("YYYY")
  console.log("year", moment(year).format("YYYY"))
  console.log("yyyyy", yearformat)



    const [rowsData, setRowsData] = useState([]);
    const [indic, setIndic] = useState([])
    const [benefit ,setBenefit] = useState([])
    const [object ,setObject] = useState([])
 
    const addTableRows = ()=>{
  
        const rowsInput={
            fullName:'',
            datestart:'',
            dateend :'' 
            
        } 
        setRowsData([...rowsData, rowsInput])
      
    }
   const deleteTableRows = (index)=>{
        const rows = [...rowsData];
        rows.splice(index, 1);
        setRowsData(rows);
   }
 
   const handleChange = (index, evnt)=>{
    
    const { name, value } = evnt.target;
    const rowsInput = [...rowsData];
    rowsInput[index][name] = value;
    setRowsData(rowsInput);
    }

   const addTabindic = ()=>{
  
    const rowsInputindic={
        indicname:'',
        count:'',
        goal:''

    } 
    setIndic([...indic, rowsInputindic])
  
    }
    const deleteTabindic = (index)=>{
        const rowsindic = [...indic];
        rowsindic.splice(index, 1);
        setIndic(rowsindic);

    }

    const handleChange2 = (index, evnt)=>{

    const { nameindic, value1 } = evnt.target;
    const rowsInputindic = [...indic];
    rowsInputindic[index][nameindic] = value1;
    setIndic(rowsInputindic);
    }

    //Tabbenefit
    const addTabbenefit = ()=>{
  
      const rowsInputbenefit={
        benefit:''
  
      } 
      setBenefit([...benefit, rowsInputbenefit])
    
      }
      const deleteTabbenefit = (index)=>{
        const rowsbenefit = [...benefit];
        rowsbenefit.splice(index, 1);
        setBenefit(rowsbenefit);

    }
  
      const handleChange3 = (index, evnt)=>{

        const { namebenefit, value2} = evnt.target;
        const rowsInputbenefit = [...benefit];
        rowsInputbenefit[index][namebenefit] = value2;
        setBenefit(rowsInputbenefit);
    }
    ////object
    const addTabobject = ()=>{
  
      const rowsInputobject={
        object:''
  
      } 
      setObject([...object, rowsInputobject])
    
      }
      const deleteTabobject = (index)=>{
        const rowsobject = [...object];
        rowsobject.splice(index, 1);
        setObject(rowsobject);

    }
  
      const handleChange4 = (index, evnt)=>{

        const { nameobject, value3} = evnt.target;
        const rowsInputobject = [...object];
        rowsInputobject[index][nameobject] = value3;
        setObject(rowsInputobject);
    }
  

  return (
    <div align="left">
    <h2>จัดการข้อมูลโครงการ</h2>
      <Card>
          <h3>สร้างโครงการใหม่</h3>
      <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ปีงบประมาณ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  <Calendar
            id="yearpicker"
            value={year}
            onChange={(e) => setYear(e.value)}
            view="year"
            dateFormat="yy"
          />
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  <Button label="ตั้งค่า" className="p-button-success" style={{ marginRight: ".9em" }}
                      />
                  </div>
                </div>
              </div>
            </div>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ชื่อโครงการ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ชื่อโครงการ :"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>หน่วยงานที่รับผิดชอบโครงการ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="หน่วยงานที่รับผิดชอบโครงการจากดาต้าเบส :"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-3">
                  <h3>ผู้รับผิดชอบโครงการ :</h3>
                </div>

                <div className="col-12 md:col-6">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ผู้รับผิดชอบโครงการชื่อคนlogin dropdowจากdbb:"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-3">
                  <div className="p-inputgroup">
                  <Button
                      label="เพิ่ม"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ชื่อแผนยุทธ์ศาสตร์:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ดรอปดาวเพิ่มได้เหมือนเป้าปรสง"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ประเภทโครงการ:</h3>
                </div>

                <div className="col-12 md:col-5">
                <div className="field-radiobutton">
                    <RadioButton inputId="typpj1" name="typpj" value="โครงการในแผน" onChange={(e) => setTyppj(e.value)} checked={typpj === 'โครงการในแผน'} />
                    <label htmlFor="typpj1">โครงการในแผน</label>
                </div>
                <div className="field-radiobutton">
                    <RadioButton inputId="typpj2" name="typpj" value="โครงการนอกแผน" onChange={(e) => setTyppj(e.value)} checked={typpj === 'โครงการนอกแผน'} />
                    <label htmlFor="typpj2">โครงการนอกแผน</label>
                </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ลักษณะโครงการ:</h3>
                </div>

                <div className="col-12 md:col-5">
                <div className="field-radiobutton">
                    <RadioButton inputId="updatepj1" name="updatepj" value="โครงการใหม่" onChange={(e) => setUpdatepj(e.value)} checked={updatepj === 'โครงการใหม่'} />
                    <label htmlFor="updatepj1">โครงการใหม่</label>
                </div>
                <div className="field-radiobutton">
                    <RadioButton inputId="updatepj2" name="updatepj" value="โครงการต่อเนื่อง" onChange={(e) => setUpdatepj(e.value)} checked={updatepj === 'โครงการต่อเนื่อง'} />
                    <label htmlFor="updatepj2">โครงการต่อเนื่อง</label>
                </div>
                <div className="field-radiobutton">
                    <RadioButton inputId="updatepj3" name="updatepj" value="งานประจำ" onChange={(e) => setUpdatepj(e.value)} checked={updatepj === 'งานประจำ'} />
                    <label htmlFor="updatepj3">งานประจำ</label>
                </div>
                <div className="field-radiobutton">
                    <RadioButton inputId="updatepj4" name="updatepj" value="งานพัฒนา" onChange={(e) => setUpdatepj(e.value)} checked={updatepj === 'งานพัฒนา'} />
                    <label htmlFor="updatepj4">งานพัฒนา</label>
                </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>การบูรณาการโครงการ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ดรอปดาวเลือกดาต้าจากดาต้าเบส :"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>หลักการและเหตุผล:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  <InputTextarea value={value1} onChange={(e) => setValue1(e.target.value)} rows={5} cols={30} />
  
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>วัตถุประสงค์ :</h3>
                </div>

               
                <div className="col-12 md:col-10">
                   <table className="table">
                    <thead>
                      <tr>
                      <th><InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="วัตถุประสงค์"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabobject} style={{ marginRight: "8px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabobject rowsData={object} deleteTableRows={deleteTabobject} handleChange4={handleChange4} style={{ marginRight: "3px" }} />
                   </tbody> 
                </table>
                  
                </div>
                </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ตัวชี้วัดความสำเร็จระดับโครงการ:</h3>
                </div>

                <div className="col-12 md:col-10">
            <table className="table">
                    <thead>
                      <tr>
                          <th>ตัวชี้วัดความสำเร็จ <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ตัวชี้วัดความสำเร็จ"
                    ></InputText></th>
                          <th>หน่วยนับ<InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="หน่วยนับ"
                    ></InputText></th>
                          <th>ค่าเป้าหมาย	<InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ค่าเป้าหมาย"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabindic} style={{ marginTop: "25px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabindic  rowsData={indic} deleteTableRows={deleteTabindic} handleChange={handleChange2} style={{ marginRight: "3px" }} />
                   </tbody> 
                </table>
                </div>

                <div className="col-12 md:col-1">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>กลุ่มเป้าหมาย:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="กลุ่มเป้าหมาย:"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ขั้นตอนการดำเนินการ :</h3>
                </div>

                <div className="col-12 md:col-9">
            <table className="table">
                    <thead>
                      <tr>
                          <th>ขั้นตอนการดำเนินการ/รายการกิจกรรม</th>
                          <th>เริ่มต้น</th>
                          <th>สิ้นสุด</th>
                          <th><Button className="btn btn-outline-success" onClick={addTableRows} style={{ marginRight: "3px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <TableRows rowsData={rowsData} deleteTableRows={deleteTableRows} handleChange={handleChange} style={{ marginRight: "3px" }} />
                   </tbody> 
                </table>
                </div>

                <div className="col-12 md:col-1">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>แหล่งเงิน/ประเภทงบประมาณที่ใช้ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ดรอปดาวเพิ่มได้เหมือนเป้าปรสง"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ประโยชน์ที่คาดว่าจะได้รับ :</h3>
                </div>

                <div className="col-12 md:col-10">
                   <table className="table">
                    <thead>
                      <tr>
                      <th><InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ประโยชน์ที่คาดว่าจะได้รับ"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabbenefit} style={{ marginRight: "8px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabbenefit rowsData={benefit} deleteTableRows={deleteTabbenefit} handleChange={handleChange3} style={{ marginRight: "3px" }} />
                   </tbody> 
                </table>
                  
                </div>

              </div>
            </div>


            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>เอกสาร TOR :</h3>
                </div>

                <div className="col-12 md:col-5">
                <div className="field-radiobutton">
                    <RadioButton inputId="have" name="have" value="มี" onChange={(e) => setHave(e.value)} checked={have === 'มี'} />
                    <label htmlFor="have1">มี</label>
                </div>
                <div className="field-radiobutton">
                    <RadioButton inputId="have" name="have" value="ไม่มี" onChange={(e) => setHave(e.value)} checked={have === 'ไม่มี'} />
                    <label htmlFor="have2">ไม่มี</label>
                </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    
                  </div>
                </div>
              </div>
            </div>
            
            <div className="col-12 md:col-6" >
          <Button
                      label="บันทึก"
                      className="p-button-success"
                      style={{ marginRight: ".1em" }}
                      // onClick={() => addstrategic(value1)}
                    />
          <Button
                      label="ส่ง"
                      className="p-button-danger"
                      style={{ marginRight: ".1em" }}
                    //   onClick={() =>
                    //     {}
                    //   }
                    />
          </div>
          </Card>
    </div>
  );
};

export default Newproject
