import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";


function Tabobject({rowsData, deleteTableRows, handleChange4}) {
    return(
        
        rowsData.map((data, index)=>{
            const {object}= data;
            return(
               
                <tr key={index}>
                <td><InputText type="text" value={object} onChange={(evnt)=>(handleChange4(index, evnt))} name="object" className="form-control" style={{ marginTop: "3px" }}/>
                </td>
                 <td><Button className="p-button-danger" onClick={()=>(deleteTableRows(index))} style={{ marginTop: "3px" }}>x</Button></td>
            </tr>
            )
        })
   
    )
    
}

export default Tabobject;