import "primeicons/primeicons.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import { Button } from "primereact/button";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Dialog } from 'primereact/dialog';
import axios from "axios";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import Editdatastg from "../add/editdatastg";

const StrategicIssues = () => {
  const [strategic, setStrategic] = useState([]);
  const [selectedSt, setSelectedSt] = useState([]);
  const [stopen, setStopen] = useState();
  const [value1, setValue1] = useState();
  const [value2, setValue2] = useState();
  const [stname, setStname] = useState();
  const [displayBasic, setDisplayBasic] = useState(false);
  const [dataUpdate, setDataUpdate] = useState("");
  const [id, setId] = useState();
  let history = useHistory();

  useEffect(() => {
    getstrategicid();
  }, []);
  const getstrategicid = () => {
    axios
      .get("http://localhost:3001/strategicid", {})
      .then((res) => {
        setStrategic(res.data);
        Stopen(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deletestid = (ID) => {
    axios.delete(`http://localhost:3001/deletestid/${ID}`);
    getstrategicid();
    alert(`Delete id${ID} sucessful`);
  };


  const updatest = (ID,dataUpdate) => {
    axios.put(`http://localhost:3001/updatest/${ID}`, { 
      strategic_name: dataUpdate
        
    }
        )
        onHide()
        getstrategicid()
    
  };

  const addstid = (stname) => {
    console.log("val", stname);
    console.log("val", value2[0].fiscalyear_id);
    try {
      axios.post("http://localhost:3001/createstid", {
        fiscalyear_id: value2[0].fiscalyear_id,
        order_strategic: value2.length + 1,
        strategic_name: stname,
      });
      // getposition()
      // setValue1('')
    } catch (e) {
      //handleError
    }
  };
  const Stopen = (m) => {
    const rows = [];
    const rowsuse = [];
    const collunm = m.find((obj) => {
      console.log("มั่ว", obj.flag);
      if (obj.flag === 1) {
        ///let a =
        rows.push(obj);
      }
    });

    setStopen(rows);
  };
  //console.log("1111",stopen)
  const onStrategic = (e) => {
    setSelectedSt(e.value);
    console.log("2222", e.value);
    const q = e.value.fiscalyear_id;
    const setst = stopen.filter((stopen) => stopen.fiscalyear_id === q);
    setValue2(setst);
  };

  const actionTemplate = (node, column) => {
    return (
      <div>
        <Button
          type="button"
          icon="pi pi-search"
          className="p-button-success"
          style={{ marginRight: ".5em" }}
        ></Button>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          style={{ marginRight: ".5em" }}
          onClick={() =>
            history.push({ pathname: "/home/stra/edit", state: node })
          }
        ></Button>
        <Button
          type="button"
          icon="pi pi-trash"
          className="p-button-danger"
          onClick={() => {
            deletestid(node.strategic_id);
          }}
        ></Button>
      </div>
    );
  };

  const dialogFuncMap = {
    displayBasic: setDisplayBasic,
  };
  const show = (id) => {
    setDisplayBasic(true);
    setId(id);
  };
  const onHide = () => {
    setDisplayBasic(false);
  };

  const action = (rowData) => {
    return (
      <div>
        <span className="button-text">{rowData.strategic_name}</span>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          style={{ marginLeft: ".5em" }}
          onClick={() => show(rowData.strategic_id)}
        ></Button>
      </div>
    );
  };

  const confirm2 = (id, dataUpdate) => {
    updatest(id,dataUpdate)
  }
  const renderFooter = () => {
    return (
    
        <div>
            <Button label="No" icon="pi pi-times" onClick={onHide} className="p-button-text" />
            <Button label="Yes" icon="pi pi-check" onClick={()=>confirm2(id,dataUpdate)} autoFocus />
        </div>
        
    
    );
}

  return (
    <div className="text-left">
      <h2>จัดการข้อมูลประเด็นยุทธ์ศาสตร์ เป้าประสงค์ กลยุทธ์</h2>
      <div className="mt-4" style={{ marginTop: "2em" }}>
        <h3>แผนยุทธศาสตร์</h3>
        <div className="dropdown-demo">
          <Dropdown
            value={selectedSt}
            options={stopen}
            onChange={onStrategic}
            optionLabel="plan_name"
            placeholder="select"
          />
        </div>
        <h3>ชื่อประเด็นยุทธศาสตร์ :</h3>
        <div className="grid p-fluid">
                <div className="col-12 md:col-4">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ประเด็นยุทธศาสตร์ "
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่ม"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      onClick={() => addstid(value1)}
                    />
                  </div>
                </div>
              </div>
        
      </div>
      <div>
        <DataTable
          value={value2}
          columnResizeMode="fit"
          showGridlines //icon={node.flag === 1 ? "pi pi-check" :"pi pi-times" }
          responsiveLayout="scroll"
          style={{ marginTop: "30px" }}
        >
          {/* <Column field="" header="ลำดับ" style={{ width: "3%" }} /> */}
          <Column field="plan_name" header="แผนยุทธศาสตร์" />
          <Column body={action} header="ชื่อประเด็นยุทธศาสตร์"></Column>
          <Column
            body={actionTemplate}
            header="จัดการ"
            style={{ textAlign: "center", width: "15%" }}
          />
        </DataTable>
      </div>
      <Dialog
        header="Header"
        visible={displayBasic}
        style={{ width: "50vw" }}
        footer={renderFooter}
        onHide={onHide}
      >
        <InputText
          value={dataUpdate}
          onChange={(e) => setDataUpdate(e.target.value)}
          placeholder="strategic_name"
          
        />
      </Dialog>
    </div>
  );
};

export default StrategicIssues;
