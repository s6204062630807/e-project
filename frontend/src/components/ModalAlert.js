import React from 'react'
import { Alert, AlertTitle, Snackbar } from '@mui/material'

const ModalAlert = ({open,setOpen,type,title,description}) => {

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        setOpen(false)
    }

    return (
        <Snackbar open={open} autoHideDuration={6000} anchorOrigin={{vertical: 'top', horizontal: 'right'}} sx={{width: '20%'}}  onClose={handleClose}>
            <Alert severity={type} sx={{ width: '100%' }}  onClose={handleClose}>
                <AlertTitle>{title}</AlertTitle>
                {description}
            </Alert>
        </Snackbar>
    )

}

export default ModalAlert
