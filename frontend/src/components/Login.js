import React, { useState } from 'react'
import axios from 'axios'
 
const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loginStatus, setLoginStatus] = useState("");

    const login = () => {
        axios.post('http://localhost:3001/auth', {
           username: username,
           password: password,
        }).then((response) => {
           if (!response.data.message) {
              setLoginStatus( response.data.message);
           } else {
              setLoginStatus (response.data[0].message);
           }
        });
        }
 
    
    return (
        <div>
            <h1>Login</h1>
            <input type="text" placeholder="username..." onChange={(e) =>{setUsername(e.target.value)}}/>
            <input type="text" placeholder="password" onChange={(e) =>{setPassword(e.target.value)}}/>
            <button onClick = {login}>Login</button>
        </div>
    )
}
 
export default Login
// import React, { useState } from 'react'
// import axios from 'axios';
// import { useHistory } from 'react-router-dom';
 
// const Login = () => {
//     const [username, setUsername] = useState('');
//     const [password, setPassword] = useState('');
//     const [msg, setMsg] = useState('');
//     const history = useHistory();
 
//     const Auth = async (e) => {
//         e.preventDefault();
//         try {
//             await axios.post('http://localhost:3001/auth', {
//                 email: username,
//                 password: password
//             });
//             history.push("/dashboard");
//         } catch (error) {
//             if (error.response) {
//                 setMsg(error.response.data.msg);
//             }
//         }
//     }
 
//     return (
        
//         <div>
//             <h1>Login</h1>
//             <input type="text" placeholder="username..." onChange={(e) =>{setUsername(e.target.value)}}/>
//             <input type="text" placeholder="password" onChange={(e) =>{setPassword(e.target.value)}}/>
//             <button onClick = {Auth}>Login</button>
//         </div>
//     )
// }
 
// export default Login
 