import React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import TreeItem from '@mui/lab/TreeItem';
import Manageuser from './manageuser'
import Managesys from './managesys'
import Newproject from './newproject'
import Strategicplan from './strategicplan'
import StrategicIssues from './strategicissues'


const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

export default function Pagehome() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [choice, setChoice] = React.useState('')

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const Test = (label) => {
    setChoice(label)
    console.log(label)
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            ฉันถ่อมากนะ
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader><h3>ระบบประเมินผล</h3>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <TreeView
      aria-label="multi-select"
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      multiSelect
    >
      <TreeItem nodeId="1" label={<p>ผู้ดูแลระบบ</p>}style={{textAlign: 'left'}}>
        <TreeItem nodeId="2" label="กำหนดสิทธิผู้ใช้" onClick = {()=>{Test('2')}} />
        <TreeItem nodeId="3" label="กำหนดโครงสร้างหน่วยงาน" onClick = {()=>{Test('3')}}/>
      </TreeItem>
      <TreeItem nodeId="4" label={<p>เจ้าหน้าที่ฝ่ายแผน</p>}style={{textAlign: 'left'}}>
      {/* <TreeItem nodeId="5" label="จัดการข้อมูลปีงบประมาน" onClick = {()=>{Test('5')}}/> */}
        <TreeItem nodeId="5" label="จัดการข้อมูลแผนยุทธ์ศาสตร์" onClick = {()=>{Test('5')}}/>
        <TreeItem nodeId="6" label="จัดการข้อมูลประเด็นยุทธ์ศาสตร์ เป้าประสงค์ กลยุทธ์"onClick = {()=>{Test('6')}} />
        <TreeItem nodeId="7" label="จัดการข้อมูลโครงการ">
        <TreeItem nodeId="8" label="รายชื่อโครงการ" onClick = {()=>{Test('8')}}/>
        <TreeItem nodeId="9" label="โครงการที่ไม่อนุมัติ"onClick = {()=>{Test('9')}} />
        </TreeItem>
        <TreeItem nodeId="10" label="จัดการรายงาน" onClick = {()=>{Test('10')}}/>
      </TreeItem>
      <TreeItem nodeId="11" label={<p>หัวหน้าฝ่าย</p>}style={{textAlign: 'left'}}>
      <TreeItem nodeId="12" label="รายชื่อโครงการ" onClick = {()=>{Test('12')}}/>
      <TreeItem nodeId="13" label="โครงการที่ไม่อนุมัติ" onClick = {()=>{Test('13')}}/>
      </TreeItem>
      <TreeItem nodeId="14" label={<p>ผู้รับผิดชอบโครงการ</p>}style={{textAlign: 'left'}}>
        <TreeItem nodeId="15" label="สร้างโครงการใหม่" onClick = {()=>{Test('15')}} />
        <TreeItem nodeId="16" label="ข้อมูลโครงการที่ยื่นเสนอ" onClick = {()=>{Test('16')}}/>
        <TreeItem nodeId="17" label="โครงการที่ไม่ผ่านอนุมัติ" onClick = {()=>{Test('17')}}/>
      </TreeItem>
      <TreeItem nodeId="19" label={<p>ผู้บริหาร</p>}style={{textAlign: 'left'}}>
        <TreeItem nodeId="20" label="พิจารณาโครงการ" onClick = {()=>{Test('2')}} />
      </TreeItem>
      <TreeItem nodeId="21" label={<p>นักวิชาการพัสดุ</p>}style={{textAlign: 'left'}}>
        <TreeItem nodeId="22" label="รายชื่อโครงการ" onClick = {()=>{Test('2')}} />
      </TreeItem>
     
    </TreeView>
      </Drawer>
      <Main open={open}>
        <DrawerHeader />
        {choice === '2' ? <Manageuser/>:null}
        {choice === '3' ? <Managesys/>:null}
        {/* {choice === '5' ? <Fiscalyear/>:null} */}
        {choice === '5' ? <Strategicplan/>:null}
        {choice === '6' ? <StrategicIssues/>:null}
        {choice === '15' ? <Newproject/>:null}
        
        
      </Main>
    </Box>
  );
}
