import React, { useState } from 'react'
import {
    Avatar,
    Box,
    Button,
    Container,
    CssBaseline,
    Grid,
    IconButton,
    InputAdornment,
    TextField,
    Typography
} from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import { Link, useHistory } from 'react-router-dom'
import * as Yup from 'yup'
import { Visibility, VisibilityOff } from '@mui/icons-material'
import ModalAlert from '../components/ModalAlert'

const Login = () => {
    let history = useHistory()
    const [showPassword, setShowPassword] = useState(false)
    const [isOpen, setIsOpen] = useState(false)

    const initialValues = {
        username: '',
        password: ''
    }

    const LoginSchema = Yup.object().shape({
        username: Yup.string()
            .required('Please enter username'),
        password: Yup.string()
            .required('Please enter password'),
    })

    const onSubmit = async (values) => {
        console.log(values)
        history.push('/home')
        // try {
        //     //    const {data} = await axios.post('http://localhost:3001/auth', {
        //     //         username: values.username,
        //     //         password: values.password
        //     //     })
        //     //     console.log('hhhh',data)
        //     //    if (data) {
        //     //     //     localStorage.setItem("ID",data.id)
        //     //     history.push('/home')
        //     //    }
        // } catch (e) {
        //     setIsOpen(true)
        //
        // }
    }

    const handleClickShowPassword = () => {
        setShowPassword((show) => !show)
    }

    const handleMouseDownPassword = (event) => {
        event.preventDefault()
    }

    const handleClose = () => {
        setIsOpen(false)
    }

    return (
        <>
            {isOpen &&
                <ModalAlert open={isOpen} setOpen={setIsOpen} type={'error'} title={'อีเมลหรือรหัสผ่านไม่ถูกต้อง'}
                            description={' กรุณาตรวจสอบอีเมลหรือรหัสผ่านใหม่อีกครั้ง'}/>}

            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        minHeight: '100vh'
                    }}
                >
                    <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Formik
                        initialValues={initialValues}
                        validationSchema={LoginSchema}
                        onSubmit={onSubmit}>
                        {({
                              errors,
                              touched
                          }) => (
                            <Box sx={{mt: 1}}>
                                <Form>
                                    <Field as={TextField} name="username" label="username" fullWidth
                                           error={errors.email && touched.email}
                                           helperText={<ErrorMessage name="username"/>} margin="normal" autoFocus
                                           required/>
                                    <Field as={TextField} name="password" label="Password"
                                           type={showPassword ? 'text' : 'password'} fullWidth
                                           error={errors.password && touched.password}
                                           InputProps={{ // <-- This is where the toggle button is added.
                                               endAdornment: (
                                                   <InputAdornment position="end">
                                                       <IconButton
                                                           aria-label="toggle password visibility"
                                                           onClick={handleClickShowPassword}
                                                           onMouseDown={handleMouseDownPassword}
                                                       >
                                                           {showPassword ? <Visibility/> : <VisibilityOff/>}
                                                       </IconButton>
                                                   </InputAdornment>
                                               )
                                           }}
                                           helperText={<ErrorMessage name="password"/>} margin="normal" required/>
                                    {/* <ThemeProvider theme={theme}> */}
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        sx={{mt: 3, mb: 2}}
                                    >
                                        Sign In
                                    </Button>
                                    {/* </ThemeProvider> */}

                                    <Grid container>
                                        <Grid item xs>
                                            {/*<Link href="#" variant="body2">*/}
                                            {/*    Forgot password?*/}
                                            {/*</Link>*/}
                                        </Grid>
                                        <Grid item>
                                            <Link to={location => ({...location, pathname: '/user/register'})}
                                                  variant="body2">
                                                {'Don\'t have an account? Sign Up'}
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </Form>
                            </Box>
                        )}
                    </Formik>
                </Box>
            </Container>
        </>

    )

}

export default Login
