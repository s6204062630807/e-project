import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";


function Tabstra({rowsData, deleteTableRows, handleChange4}) {
    return(
        rowsData.map((data, index)=>{
            const {stra}= data;
            return(
               
                <tr key={index}>
                <td><InputText type="text" placeholder="กลยุทธ์" value={stra} onChange={(evnt)=>(handleChange4(index, evnt))} name="stra" className="form-control" style={{ marginTop: "3px" }}/>
                </td>
                 <td><Button className="p-button-danger" onClick={()=>(deleteTableRows(index))} style={{ marginTop: "3px" }}>x</Button></td>
            </tr>
            )
        })
    )
    
}

export default Tabstra;