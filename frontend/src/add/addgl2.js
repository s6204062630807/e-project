import React from "react"
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Card } from "primereact/card";

const Addgl = () => {
    return(
        <div className="card">
          <Card>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ตัวชี้วัด:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ตัวชี้วัด"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มตัวชี้วัด"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>หน่วยนับ:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="หน่วยนับ"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ค่าเป้าหมาย:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ค่าเป้าหมาย"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>กลยุทธ์:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="กลยุทธ์"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มกลยุทธ์"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </div>
    )
}

export default Addgl