import React, { useState } from 'react';

function MultiInputText() {
  // Use the useState hook to create an array of input values
  // and a function to update them
  const [inputValues, setInputValues] = useState([]);

  const handleChange = (index, event) => {
    // Make a copy of the inputValues array
    const newInputValues = [...inputValues];

    // Update the value of the input at the specified index
    newInputValues[index] = event.target.value;

    // Update the state with the new inputValues array
    setInputValues(newInputValues);
  };

  const handleAdd = () => {
    // Add an empty string to the inputValues array
    setInputValues([...inputValues, '']);
  };

  const handleRemove = (index) => {
    // Make a copy of the inputValues array
    const newInputValues = [...inputValues];

    // Remove the input at the specified index
    newInputValues.splice(index, 1);

    // Update the state with the new inputValues array
    setInputValues(newInputValues);
  };

  return (
    <div>


      <button type="button" onClick={handleAdd}>
        Add
      </button>
      {inputValues.map((value, index) => (
        <div key={index}>
          <input
            type="text"
            value={value}
            onChange={(event) => handleChange(index, event)}
          />
          <button type="button" onClick={() => handleRemove(index)}>
            Remove
          </button>
        </div>
      ))}
      
    </div>
  );
}

export default MultiInputText;