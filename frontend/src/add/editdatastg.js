import React from "react";
import { useHistory } from "react-router-dom";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import TreeItem from "@mui/lab/TreeItem";
import Manageuser from "../components/manageuser";
import Managesys from "../components/managesys";
import Fiscalyear from "../components/newproject";
import Strategicplan from "../components/strategicplan";
import StrategicIssues from "../components/strategicissues";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import Tabstra from "../add/Tabstra"
import Tabindicator from "../add/Tabindicator"



const drawerWidth = 240;

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  })
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

export default function Editdatastg() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [choice, setChoice] = React.useState("");
  const [value1, setValue1] = React.useState([]);
  let history = useHistory();
  const [stra, setStra] = React.useState([])
  const [indicator, setIndicator] = React.useState([])
  
  const test = () =>{
   
  }


  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const Test = (label) => {
    setChoice(label);
    console.log(label);
  };

///stra-------
  const addTabstra = ()=>{
  
    const rowsInputstra={
      stra:''

    } 
    setStra([...stra, rowsInputstra])
  
    }
    const deleteTabstra = (index)=>{
      const rowsstra = [...stra];
      rowsstra.splice(index, 1);
      setStra(rowsstra);

  }

    const handleChange = (index, evnt)=>{

      const { namestra, value} = evnt.target;
      const rowsInputstra = [...stra];
      rowsInputstra[index][namestra] = value;
      setStra(rowsInputstra);
  }

  ///ตัวชี้วัด
  
  const addTabindicator = ()=>{
  
    const rowsInputindicator={
      indicatorname:'',
      count:'',
      goal:''

    } 
    setIndicator([...indicator, rowsInputindicator])
  
    }
    const deleteTabindicator= (index)=>{
      const rowsindicator = [...indicator];
      rowsindicator.splice(index, 1);
      setIndicator(rowsindicator);

  }

    const handleChange2 = (index, evnt)=>{

      const { nameindicator, value2} = evnt.target;
      const rowsInputindicator = [...indicator];
      rowsInputindicator[index][nameindicator] = value2;
      setIndicator(rowsInputindicator);
  }

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: "none" }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            ฉันถ่อมากนะ
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <h3>ระบบประเมินผล</h3>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <TreeView
          aria-label="multi-select"
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
          multiSelect
        >
          <TreeItem
            nodeId="1"
            label={<p>ผู้ดูแลระบบ</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem
              nodeId="2"
              label="กำหนดโครงสร้างหน่วยงาน"
              onClick={() => {
                Test("2");
              }}
            />
            <TreeItem
              nodeId="3"
              label="กำหนดสิทธิผู้ใช้"
              onClick={() => {
                Test("3");
              }}
            />
          </TreeItem>
          <TreeItem
            nodeId="4"
            label={<p>เจ้าหน้าที่ฝ่ายแผน</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem
              nodeId="5"
              label="จัดการข้อมูลปีงบประมาน"
              onClick={() => {
                Test("5");
              }}
            />
            <TreeItem
              nodeId="6"
              label="จัดการข้อมูลแผนยุทธ์ศาสตร์"
              onClick={() => {
                Test("6");
              }}
            />
            <TreeItem
              nodeId="7"
              label="จัดการข้อมูลประเด็นยุทธ์ศาสตร์ เป้าประสงค์ กลยุทธ์"
              onClick={() => {
                Test("7");
              }}
            />
            <TreeItem nodeId="8" label="จัดการข้อมูลโครงการ">
              <TreeItem
                nodeId="9"
                label="รายชื่อโครงการ"
                onClick={() => {
                  Test("9");
                }}
              />
              <TreeItem
                nodeId="10"
                label="โครงการที่ไม่อนุมัติ"
                onClick={() => {
                  Test("10");
                }}
              />
            </TreeItem>
            <TreeItem
              nodeId="11"
              label="จัดการรายงาน"
              onClick={() => {
                Test("11");
              }}
            />
          </TreeItem>
          <TreeItem
            nodeId="12"
            label={<p>หัวหน้าฝ่าย</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem nodeId="13" label="รายชื่อโครงการ" />
            <TreeItem nodeId="14" label="โครงการที่ไม่อนุมัติ" />
          </TreeItem>
          <TreeItem
            nodeId="15"
            label={<p>ผู้รับผิดชอบโครงการ</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem
              nodeId="16"
              label="สร้างโครงการใหม่"
              onClick={() => {
                Test("2");
              }}
            />
            <TreeItem
              nodeId="17"
              label="ข้อมูลโครงการที่ยื่นเสนอ"
              onClick={() => {
                Test("3");
              }}
            />
            <TreeItem
              nodeId="18"
              label="โครงการที่ไม่ผ่านอนุมัติ"
              onClick={() => {
                Test("3");
              }}
            />
          </TreeItem>
          <TreeItem
            nodeId="19"
            label={<p>ผู้บริหาร</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem
              nodeId="20"
              label="พิจารณาโครงการ"
              onClick={() => {
                Test("2");
              }}
            />
          </TreeItem>
          <TreeItem
            nodeId="21"
            label={<p>นักวิชาการพัสดุ</p>}
            style={{ textAlign: "left" }}
          >
            <TreeItem
              nodeId="22"
              label="รายชื่อโครงการ"
              onClick={() => {
                Test("2");
              }}
            />
          </TreeItem>
        </TreeView>
      </Drawer>
      <Main open={open}>
        <DrawerHeader />
        {/* {choice === '2' ? <Manageuser/>:null}
        {choice === '3' ? <Managesys/>:null}
        {choice === '5' ? <Fiscalyear/>:null}
        {choice === '6' ? <Strategicplan/>:null} */}
        {/* {choice === '7' ? <StrategicIssues/>:null} */}

        <h2>สร้างเป้าประสงค์ใหม่</h2>
        <div className="card">
          <Card>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>เป้าประสงค์ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="เป้าประสงค์"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มเป้าประสงค์"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      onClick={test}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ตัวชี้วัด:</h3>
                </div>

                <div className="col-12 md:col-10">
                <table className="table">
                    <thead>
                      <tr>
                      <th><InputText
                      value={stra}
                      onChange={(e) => setStra(e.target.value)}
                      placeholder="ตัวชี้วัด"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabindicator} style={{ marginRight: "8px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabindicator rowsData={indicator} deleteTableRows={deleteTabindicator} handleChange={handleChange2} style={{ marginRight: "10px" }} />
                   </tbody> 
                </table>
                </div>

                
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>หน่วยนับ:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="หน่วยนับ"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ค่าเป้าหมาย:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ค่าเป้าหมาย"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>กลยุทธ์:</h3>
                </div>

                <div className="col-12 md:col-10">
               
                <table className="table">
                    <thead>
                      <tr>
                      <th><InputText
                      value={value1}
                      onChange={(e) => setStra(e.target.value)}
                      placeholder="กลยุทธ์"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabstra} style={{ marginRight: "8px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabstra rowsData={stra} deleteTableRows={deleteTabstra} handleChange={handleChange} style={{ marginRight: "10px" }} />
                   </tbody> 
                </table>
               
                </div>

                
              </div>
            </div>
          </Card>
          <div className="col-12 md:col-6" >
          <Button
                      label="บันทึก"
                      className="p-button-success"
                      style={{ marginRight: "3em" }}
                      // onClick={() => addstrategic(value1)}
                    />
          <Button
                      label="ยกเลิก"
                      className="p-button-danger"
                      style={{ marginRight: ".3em" }}
                      onClick={() =>
                        history.push({ pathname: "/home/stra" })
                      }
                    />
          </div>
          
        </div>
      </Main>
    </Box>
  );
}
