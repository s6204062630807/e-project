import React from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import Tabstra from "../add/Tabstra"
import { Card } from "primereact/card";


function Tabindicator({rowsData, deleteTableRows, handleChange1}) {
    const [stra, setStra] = React.useState([])

    const addTabstra = ()=>{
  
        const rowsInputstra={
          stra:''
    
        } 
        setStra([...stra, rowsInputstra])
      
        }
        const deleteTabstra = (index)=>{
          const rowsstra = [...stra];
          rowsstra.splice(index, 1);
          setStra(rowsstra);
    
      }
    
        const handleChange = (index, evnt)=>{
    
          const { namestra, value} = evnt.target;
          const rowsInputstra = [...stra];
          rowsInputstra[index][namestra] = value;
          setStra(rowsInputstra);
      }

    return(
        
        rowsData.map((data, index)=>{
            const { indicatorname,count,goal,stra}= data;
            return(
                
                <tr key={index}>
                    
                <td><h4>ตัวชี้วัด:</h4>
                <InputText type="text" placeholder="ตัวชี้วัด" value={indicatorname} onChange={(evnt)=>(handleChange1(index, evnt))} name="indicatorname" className="form-control" style={{ marginTop: "3px" }}
                    ></InputText>
                <InputText type="text" placeholder="หน่วยนับ" value={count} onChange={(evnt)=>(handleChange1(index, evnt))} name="count" className="form-control" style={{ marginTop: "3px" }}
                    ></InputText>
                    <InputText type="text" placeholder="ค่าเป้าหมาย" value={goal} onChange={(evnt)=>(handleChange1(index, evnt))} name="goal" className="form-control" style={{ marginTop: "3px" }}
                    ></InputText>
                
            
            
            
            
            {/* <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>กลยุทธ์:</h3>
                </div>

                <div className="col-12 md:col-5">
                <table className="table">
                    <thead>
                      <tr>
                      <th><InputText
                      value={stra}
                      onChange={(e) => setStra(e.target.value)}
                      placeholder="กลยุทธ์"
                    ></InputText></th>
                          <th><Button className="btn btn-outline-success" onClick={addTabstra} style={{ marginRight: "8px" }} >+</Button></th>
                      </tr>
                    </thead>
                   <tbody>
                   <Tabstra rowsData={stra} deleteTableRows={deleteTabstra} handleChange={handleChange} style={{ marginRight: "10px" }} />
                   </tbody> 
                </table>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    
                  </div>
                </div>
              </div>
            </div> */}
          
            
            {/* <InputText
                      value={stra}
                      onChange={(e) => setStra(e.target.value)}
                      placeholder="กลยุทธ์"
                    ></InputText><Button className="btn btn-outline-success" onClick={addTabstra} style={{ marginRight: "8px" }} >+</Button> */}
            </td>
                 <td><Button className="p-button-danger" onClick={()=>(deleteTableRows(index))} style={{ marginTop: "170px" }}>x</Button></td>
            </tr>
                
               
            )
        })
   
    )
    
}

export default Tabindicator;