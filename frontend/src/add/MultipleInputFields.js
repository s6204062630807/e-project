import React, { useEffect, useRef, useState } from 'react'
import { InputText } from 'primereact/inputtext'
import { Button } from 'primereact/button'
import { Dropdown } from 'primereact/dropdown'
import { Card } from 'primereact/card'
import axios from 'axios'
import { Form } from 'antd'
import { Toast } from 'primereact/toast'

const {ThaiBaht} = require('thai-baht-text-ts') // for ES5


const AddRemoveMultipleInputFields = () => {
    const [form] = Form.useForm()
    const toast = useRef(null)
    const [workplan, setWorkplan] = useState([])
    const [selectworkplan, setSelectworkplan] = useState()
    const [money, setMoney] = useState()
    const pie = parseFloat(money, 10)
    const moneyText = ThaiBaht(pie)
    const [inputFields, setInputFields] = useState([
        {
            exbudget: '',
            Category: '',
            Quarter1: '',
            Quarter2: '',
            Quarter3: '',
            Quarter4: '',
        },
    ])

    useEffect(() => {
        getworkplan()
    }, [])

    console.log('money', inputFields)
    // console.log('money2', moneyText)
    const getworkplan = () => {
        axios
            .get('http://localhost:3001/workplan', {})
            .then((res) => {
                setWorkplan(res.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }


    const addInputField = () => {
        setInputFields([
            ...inputFields,
            {
                exbudget: '',
                Category: '',
                Quarter1: '',
                Quarter2: '',
                Quarter3: '',
                Quarter4: '',
            },
        ])
    }


    // console.log("inputFieldshhhh",inputFields)
    const handleMoney = (e) => {
        setMoney(e.target.value)
        form.setFieldsValue({amount: e.target.value.replace(/[^0-9]*$/, '')})
    }

    const removeInputFields = (index) => {
        const rows = [...inputFields]
        rows.splice(index, 1)
        setInputFields(rows)
    }
    const handleChange = (index, evnt) => {
        const {name, value} = evnt.target
        const list = [...inputFields]
        list[index][name] = value
        setInputFields(list)
    }

    const onFinish = async (value) => {
        console.log('input',inputFields)
        console.log('inputLenght',inputFields.length)
        console.log('value',value)
        let amount = 0
        for(const item of inputFields){
            amount += Number(item.Quarter1)+Number(item.Quarter2)+Number(item.Quarter3)+Number(item.Quarter4)
        }
        console.log('amm',amount)
        if(amount <= value.amount){
            // save into database
            toast.current.show({severity:'success', summary: 'Success', detail:'บันทึกสำเร็จ', life: 3000})
        }else {
            toast.current.show({severity:'error', summary: 'Error', detail:'รายจ่ายมากกว่าปริมาณการงบประมาณ', life: 3000})
        }
    }

    return (
        <div className="container">
            <Toast ref={toast} />
            <Card>
                <Form
                    form={form}
                    onFinish={onFinish}
                    // onFinishFailed={onFinishFailed}
                    layout="horizontal"
                >
                    <div className="fit">
                        <div className="grid p-fluid">
                            <div className="col-12 md:col-3">
                                <h3>ปริมาณการงบประมาณที่ใช้ :</h3>
                            </div>
                            <Form.Item
                                //label="เลขที่เอกสาร"
                                name="amount"
                                rules={[
                                    {
                                        required: true,
                                        message: 'กรุณากรอกปริมาณการงบประมาณ',
                                    },
                                ]}
                            >
                                <div className="col-12 md:col-3">
                                    {/*    <div className="p-inputgroup">*/}
                                    <InputText
                                        style={{width: 260}}
                                        placeholder="จำนวนเงิน "
                                        onChange={handleMoney}
                                    ></InputText>
                                    {/*<InputNumber id="number-input" value={money} onValueChange={(e) => setMoney(e.value)} />*/}
                                    {/*</div>*/}

                                </div>
                            </Form.Item>
                            <div className="col-12 md:col-3">
                                <div className="p-inputgroup">
                                    {/*{money && <InputText //disabled placeholder={moneyText}*/}
                                    {/*    disabled*/}
                                    {/*    placeholder={moneyText}*/}
                                    {/*></InputText>}*/}
                                    {money && <h3>{moneyText}</h3>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="fit">
                        <div className="grid p-fluid">
                            <div className="col-12 md:col-3">
                                <h3>แผนงาน :</h3>
                            </div>
                            <Form.Item
                                // label="จังหวัด"
                                name="province"
                                // rules={[
                                //     {
                                //         required: true,
                                //         message: 'กรุณาเลือกจังหวัด',
                                //     }
                                // ]}
                            >
                                <div className="col-12 md:col-6">
                                    {/*<div className="p-inputgroup">*/}
                                    <Dropdown
                                        style={{width: 360}}
                                        value={selectworkplan}
                                        onChange={(e) => setSelectworkplan(e.value)}
                                        options={workplan}
                                        optionLabel="workplan_name"
                                        placeholder="Select "
                                    />
                                    {/*</div>*/}
                                </div>
                            </Form.Item>
                            <div className="col-12 md:col-3">
                                <div className="p-inputgroup"></div>
                            </div>
                        </div>
                    </div>
                    <h3>ประเภทการใช้จ่าย </h3>
                    {/* <thead>
        <tr>
            <td class="text-center"> </td>
            <td class="text-center">ไตรมาส 1</td>
            <td class="text-center">ไตรมาส 2</td>
            <td class="text-center">ไตรมาส 3</td>
            <td class="text-center">ไตรมาส 4</td>
            <td class="text-center">  </td>
        </tr>
        <tr>
            <td class="text-center">ประเภทรายจ่าย</td>
            <td class="text-center">แผนการใช้จ่าย</td>
            <td class="text-center">แผนการใช้จ่าย</td>
            <td class="text-center">แผนการใช้จ่าย</td>
            <td class="text-center">แผนการใช้จ่าย</td>
            <td class="text-center"></td>
        </tr>
      </thead>  */}

                    <div className="col-12 md:col-3">
                        <></>
                        <div className="col-12 md:col-9">
                            {inputFields.map((data, index) => {
                                const {exbudget, Category, Quarter1, Quarter2, Quarter3, Quarter4} = data
                                return (
                                    <>

                                        <thead key={index}>

                                        <tr>
                                            <div className="form-group">
                                                <InputText
                                                    type="text"
                                                    onChange={(evnt) => handleChange(index, evnt)}
                                                    value={exbudget}
                                                    name="exbudget"
                                                    className="form-control"
                                                    placeholder="งบรายจ่าย"
                                                    style={{marginTop: '10px'}}
                                                    style={{marginRight: '3px'}}
                                                />
                                            </div>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr className="row my-3">
                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>

                                                <div className="form-group">
                                                    <InputText
                                                        type="text"
                                                        onChange={(evnt) => handleChange(index, evnt)}
                                                        value={Category}
                                                        name="Category"
                                                        className="form-control"
                                                        placeholder="หมวดรายจ่าย"
                                                        style={{marginTop: '10px'}}
                                                        style={{marginRight: '3px'}}
                                                    />
                                                </div>
                                            </td>

                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>
                                                <div className="form-group">
                                                    <InputText
                                                        type="number"
                                                        onChange={(evnt) => handleChange(index, evnt)}
                                                        value={Quarter1}
                                                        name="Quarter1"
                                                        className="form-control"
                                                        placeholder="Quarter1"
                                                        style={{marginTop: '10px'}}
                                                        style={{marginRight: '3px'}}
                                                    />
                                                </div>
                                            </td>


                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>
                                                <InputText
                                                    type="number"
                                                    onChange={(evnt) => handleChange(index, evnt)}
                                                    value={Quarter2}
                                                    name="Quarter2"
                                                    className="form-control"
                                                    placeholder="Quarter2"
                                                    style={{marginTop: '10px'}}
                                                    style={{marginRight: '3px'}}
                                                />
                                            </td>
                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>
                                                <InputText
                                                    type="number"
                                                    onChange={(evnt) => handleChange(index, evnt)}
                                                    value={Quarter3}
                                                    name="Quarter3"
                                                    className="form-control "
                                                    placeholder="Quarter3"
                                                    style={{marginTop: '10px'}}
                                                    style={{marginRight: '3px'}}
                                                />
                                            </td>
                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>
                                                <InputText
                                                    type="number"
                                                    onChange={(evnt) => handleChange(index, evnt)}
                                                    value={Quarter4}
                                                    name="Quarter4"
                                                    className="form-control "
                                                    placeholder="Quarter4"
                                                    style={{marginTop: '10px'}}
                                                    style={{marginRight: '3px'}}
                                                />
                                            </td>
                                            <td style={{marginTop: '10px'}}
                                                style={{marginRight: '3px'}}>
                                                {inputFields.length !== 1 ? (
                                                    <Button
                                                        className="p-button-danger"
                                                        onClick={() => (removeInputFields(index))}

                                                        style={{marginTop: '4px'}}
                                                        style={{marginRight: '3px'}}
                                                    >
                                                        X ลบ
                                                    </Button>
                                                ) : (
                                                    ''
                                                )}
                                            </td>
                                        </tr>
                                        </thead>
                                    </>
                                )
                            })}
                            <div className="row">
                                <div className="col-sm-12">
                                    <Button
                                        className="btn btn-outline-success "
                                        onClick={addInputField}
                                        style={{marginTop: '50px'}}
                                        style={{marginRight: '3px'}}
                                    >
                                        + เพิ่ม
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex sm:flex-column sm:align-items-end ">
                        <Button
                            type="primary"
                            size="large"
                            htmlType="submit"
                        >
                            Submit
                        </Button>

                    </div>

                </Form>
            </Card>
        </div>
    )
}
export default AddRemoveMultipleInputFields
