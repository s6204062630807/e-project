import React, { useState } from 'react';

import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Card } from "primereact/card";

const Addgl = () => {
  const [value1, setValue1] = React.useState();
  
    // Use the useState hook to create an array of input values
    // and a function to update them
    const [inputValues, setInputValues] = useState([]);
  
    const handleChange = (index, event) => {
      // Make a copy of the inputValues array
      const newInputValues = [...inputValues];
  
      // Update the value of the input at the specified index
      newInputValues[index] = event.target.value;
  
      // Update the state with the new inputValues array
      setInputValues(newInputValues);
    };
  
    const handleAdd = () => {
      // Add an empty string to the inputValues array
      setInputValues([...inputValues, '']);
    };
  
    const handleRemove = (index) => {
      // Make a copy of the inputValues array
      const newInputValues = [...inputValues];
  
      // Remove the input at the specified index
      newInputValues.splice(index, 1);
  
      // Update the state with the new inputValues array
      setInputValues(newInputValues);
    };
      return(
        <div className="card">
          <Card>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>เป้าประสงค์ :</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="เป้าประสงค์"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มเป้าประสงค์"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ตัวชี้วัด:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ตัวชี้วัด"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มตัวชี้วัด"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
            </div>
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>หน่วยนับ:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="หน่วยนับ"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>

            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>ค่าเป้าหมาย:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="ค่าเป้าหมาย"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div className="fit">
              <div className="grid p-fluid">
                <div className="col-12 md:col-2">
                  <h3>กลยุทธ์:</h3>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <InputText
                      value={value1}
                      onChange={(e) => setValue1(e.target.value)}
                      placeholder="กลยุทธ์"
                    ></InputText>
                  </div>
                </div>

                <div className="col-12 md:col-5">
                  <div className="p-inputgroup">
                    <Button
                      label="เพิ่มกลยุทธ์"
                      className="p-button-success"
                      style={{ marginRight: ".9em" }}
                      type="button" onClick={handleAdd}
                      // onClick={() => addstrategic(value1)}
                    />
                  </div>
                </div>
              </div>
              {inputValues.map((value, index) => (
        <div key={index}>
          <InputText
            type="text"
            value={value}
            onChange={(event) => handleChange(index, event)}
          />
          <Button type="button" onClick={() => handleRemove(index)}>
            Remove
          </Button>
        </div>
      ))}
            </div>
          </Card>
         
      
        </div>
    )
}

export default Addgl