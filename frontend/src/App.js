import './App.css';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import Pagehome from './components/Home'
import Addgl from './add/addgl'
import Editdatastg from './add/editdatastg'
import Login from './Pages/Login'

function App() {
  return (
      <Router>
        <Switch>
          <Route exact path="/user/login" render={(props) => (<Login/> )}></Route>
          <Route path="/home" render={(props) => (<Pagehome/> )}></Route>
          <Route path="/home/stra/edit" render={(props) => (<Editdatastg/>)}></Route>
          <Route path="/home/stra" render={(props) => (<Pagehome/>)}></Route>
          <Route path="/home/rrr" render={(props) => (<Addgl/>)}></Route>
          {/* <Route path="/verify" render={(props) => (<VerifyIdentity/>)}></Route>
            <Route path="/home" render={(props) => (<TopNevbar/>)}></Route>
            <Route path="/migrate" render={(props) => (<MigratePlanTable/>)}></Route>
            <Route path="/users" render={(props) => (<UserInDomain/>)}></Route>
            <Route path="/user/me" render={(props) => (<AccountDetails/>)}></Route>
            <Route path="/details/migrate/:id" render={(props) => (<DetailsMigration/>)}></Route> */}
          <Redirect to={{pathname: '/user/login'}}/>
        </Switch>
      </Router>
  );
}

export default App;
